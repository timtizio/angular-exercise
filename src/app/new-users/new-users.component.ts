import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';
import { ListsUsersComponent } from '../lists-users/lists-users.component';

import { User } from '../models/user';

@Component({
  selector: 'app-new-users',
  templateUrl: './new-users.component.html',
  styleUrls: ['./new-users.component.css']
})
export class NewUsersComponent implements OnInit {

  newUser: User;
  newUserForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });

  constructor(
    public dialogRef: MatDialogRef<ListsUsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User) { }

  ngOnInit() {
    this.newUser = {firstName: '', lastName: ''};
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onPressEnter(): void {
    this.dialogRef.close(this.newUser);
  }

}
