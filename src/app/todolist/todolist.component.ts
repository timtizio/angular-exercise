import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Todo } from '../models/todo';
import { NewTodoComponent } from '../new-todo/new-todo.component';
import { StorageTodosService } from '../storage-todos.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {

  dataSource: MatTableDataSource<Todo>;
  newTodo: Todo = new Todo();
  isEmpty: boolean;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns: string[] = ['label', 'state', 'remove'];

  constructor(
    public dialog: MatDialog,
    private storageTodosService: StorageTodosService) { }
    public progressValue: number;

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.storageTodosService.getTodos());
    this.dataSource.sort = this.sort;

    this.isEmpty = (this.dataSource.data.length === 0);

    this.reloadProgressBar();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NewTodoComponent, {
      width: '500px',
      data: this.newTodo
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result || result.label === '') {
        return;
      }

      const data = this.dataSource.data;
      data.push(result);

      this.dataSource.data = data;

      this.storageTodosService.setTodos(data);
      this.isEmpty = (this.dataSource.data.length === 0);
      this.reloadProgressBar();
    });
  }

  onClickTodo(todo: Todo): void {
    todo.state = !todo.state;

    this.storageTodosService.setTodos(this.dataSource.data);
    this.reloadProgressBar();
  }

  removeTodo(todoToRemove: Todo): void {
    console.log('removeTodo()');
    const data = this.dataSource.data;

    if (!todoToRemove.state) {
      return;
    }

    const dataFiltered = data.filter((todo, index) => {
      if (todo.label === todoToRemove.label
        && todo.state === todoToRemove.state) {
          return false;
        }
      return true;
    });

    this.dataSource.data = dataFiltered;
    this.storageTodosService.setTodos(dataFiltered);

    this.reloadProgressBar();
    this.isEmpty = (this.dataSource.data.length === 0);
  }

  private nbTodoDone(): number {
    let nbTodoDone = 0;
    for (const todo of this.dataSource.data) {
      if (todo.state) {
        nbTodoDone++;
      }
    }

    return nbTodoDone;
  }

  private reloadProgressBar() {
    this.progressValue = (this.nbTodoDone() / this.dataSource.data.length) * 100;
  }

}
