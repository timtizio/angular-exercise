import { TestBed } from '@angular/core/testing';

import { StorageUsersService } from './storage-users.service';

describe('StorageUsersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorageUsersService = TestBed.get(StorageUsersService);
    expect(service).toBeTruthy();
  });
});
