import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TodolistComponent } from '../todolist/todolist.component';

import { Todo } from '../models/todo';

@Component({
  selector: 'app-new-todo',
  templateUrl: './new-todo.component.html',
  styleUrls: ['./new-todo.component.css']
})
export class NewTodoComponent implements OnInit {
  newTodo: Todo;

  constructor(
    public dialogRef2: MatDialogRef<TodolistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Todo) { }

  ngOnInit() {
    this.newTodo = new Todo();
  }

  onNoClick(): void {
    this.dialogRef2.close();
  }

  onPressEnter(): void {
    this.dialogRef2.close(this.newTodo);
  }

}
