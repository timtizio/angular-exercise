import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListsUsersComponent } from './lists-users/lists-users.component';
import { NewUsersComponent } from './new-users/new-users.component';
import { TodolistComponent } from './todolist/todolist.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'users',
    component: ListsUsersComponent
  },
  {
    path: 'users/new',
    component: NewUsersComponent
  },
  {
    path: 'todo',
    component: TodolistComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
