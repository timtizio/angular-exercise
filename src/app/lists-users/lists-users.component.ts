import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { User } from '../models/user';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import { NewUsersComponent } from '../new-users/new-users.component';
import { StorageUsersService } from '../storage-users.service';

@Component({
  selector: 'app-lists-users',
  templateUrl: './lists-users.component.html',
  styleUrls: ['./lists-users.component.css']
})
export class ListsUsersComponent implements OnInit {

  dataSource: MatTableDataSource<User>;
  newUser: User;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns: string[] = ['firstName', 'lastName', 'remove'];

  constructor(
    public dialog: MatDialog,
    private storageUserService: StorageUsersService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.storageUserService.getUsers());
    this.dataSource.sort = this.sort;
    this.newUser = {firstName: '', lastName: ''};

    if (this.dataSource.data.length === 0) {
      console.log('NO DATA');
      // TODO: show error message
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NewUsersComponent, {
      width: '500px',
      data: {firstName: this.newUser.firstName, lastName: this.newUser.lastName}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result || result.lastName === '' || result.firstName === '') {
        return;
      }

      const data = this.dataSource.data;
      data.push(result);

      this.dataSource.data = data;

      this.storageUserService.setUsers(data);
    });
  }

  removeUser(userToRemove): void {
    console.log('removeUser()');
    const data = this.dataSource.data;

    const datafiltered = data.filter((user, index) => {
      if (user.firstName === userToRemove.firstName
        && user.lastName === userToRemove.lastName) {
          console.log('false returned');
          return false;
      }
      return true;
    });

    this.dataSource.data = datafiltered;
    this.storageUserService.setUsers(datafiltered);
  }

}
