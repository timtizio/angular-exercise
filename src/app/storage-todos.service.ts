import { Injectable } from '@angular/core';
import { Todo } from './models/todo';

@Injectable({
  providedIn: 'root'
})
export class StorageTodosService {

  STORAGE_KEY = 'user';

  constructor() { }

  getTodos(): Todo[] {
    const todos = JSON.parse(localStorage.getItem(this.STORAGE_KEY));

    return todos ? todos : [];
  }

  setTodos(todos: Todo[]): void {
    localStorage.setItem(this.STORAGE_KEY, JSON.stringify(todos));
  }

  clear(): void {
    localStorage.clear();
  }
}
