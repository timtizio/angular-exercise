import { Injectable } from '@angular/core';
import { User } from './models/user';

@Injectable({
  providedIn: 'root'
})
export class StorageUsersService {

  STORAGE_KEY = 'users';

  constructor() { }

  getUsers(): User[] {
    const users = JSON.parse(localStorage.getItem(this.STORAGE_KEY));

    return users ? users : [];
  }

  setUsers(users: User[]): void {
    localStorage.setItem(this.STORAGE_KEY, JSON.stringify(users));
  }

  clear(): void {
    localStorage.clear();
  }
}
