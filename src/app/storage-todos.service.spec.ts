import { TestBed } from '@angular/core/testing';

import { StorageTodosService } from './storage-todos.service';

describe('StorageTodosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorageTodosService = TestBed.get(StorageTodosService);
    expect(service).toBeTruthy();
  });
});
